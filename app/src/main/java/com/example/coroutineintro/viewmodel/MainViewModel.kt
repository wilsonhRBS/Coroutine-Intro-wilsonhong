package com.example.coroutineintro.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.coroutineintro.model.MainRepo
import com.example.coroutineintro.view.MainStates
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel: ViewModel() {
    private val repo = MainRepo

    private var _color: MutableLiveData<Int> = MutableLiveData<Int>()
    val color: LiveData<Int> get() = _color

    private val _state = MutableLiveData(MainStates())
    val state: LiveData<MainStates> get() = _state

    private var _studyTopics: MutableLiveData<HashMap<String, String>> = MutableLiveData<HashMap<String,String>>()
    val studyTopics: LiveData<HashMap<String, String>> get() = _studyTopics

    fun getRandomColor() = viewModelScope.launch(Dispatchers.Main){
        _state.value = _state.value!!.copy(
            enableColorSpecific = false,
            enableNavButtons = false,
            enableLifecycleSpecific = false,
            isLoading = true
        )

        _color.value = repo.getRandomColor()

        _state.value = _state.value!!.copy(
            enableColorSpecific = true,
            enableNavButtons = true,
            isLoading = false
        )
    }

    fun getTopics() = viewModelScope.launch(Dispatchers.Main){
        _state.value = _state.value!!.copy(
            enableColorSpecific = false,
            enableNavButtons = false,
            enableLifecycleSpecific = false,
            isLoading = true
        )

        _studyTopics.value = repo.getTopics()

        _state.value = _state.value!!.copy(
            enableNavButtons = true,
            enableLifecycleSpecific = true,
            isLoading = false
        )
    }

    fun getDesc(name: String): String {
        return studyTopics.value!![name].toString()
    }
}