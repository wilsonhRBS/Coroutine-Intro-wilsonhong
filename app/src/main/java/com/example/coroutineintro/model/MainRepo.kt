package com.example.coroutineintro.model

import android.content.res.Resources
import android.graphics.Color
import com.example.coroutineintro.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlin.random.Random

object MainRepo {
    private val mainApi: MainApi = object: MainApi{
        override suspend fun getDisplayTopics(): HashMap<String, String> = mapOf(
            "1. onAttach" to "Called when a fragment is first attached to its context",
            "2. onCreate" to "Called to do initial creation of a fragment",
            "3. onCreateView" to "Called to have the fragment instantiate its user interface view",
            "4. onViewCreated" to "Called immediately after onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle) has returned, but before any saved state has been restored in to the view.",
            "5. onStart" to "Called when the Fragment is visible to the user",
            "6. onResume" to "Called when the fragment is visible to the user and actively running",
            "7. onPause" to "Called when the Fragment is no longer resumed",
            "8. onStop" to "Called when the Fragment is no longer started",
            "9. onDestroyView" to "Called when the view previously created by onCreateView(LayoutInflater, ViewGroup, Bundle) has been detached from the fragment",
            "10. onDestroy" to "Called when the fragment is no longer in use",
            "11. onDetach" to "Called when the fragment is no longer attached to its activity") as HashMap<String, String>

        override suspend fun getRandomColor(): Int {
            return Color.rgb(Random.nextInt(255), Random.nextInt(255), Random.nextInt(255))
        }
    }

    suspend fun getTopics(): HashMap<String, String> = withContext(Dispatchers.IO) {
        delay(1000)
        return@withContext mainApi.getDisplayTopics()
    }

    suspend fun getRandomColor(): Int = withContext(Dispatchers.IO){
        delay(1000)
        return@withContext mainApi.getRandomColor()
    }
}