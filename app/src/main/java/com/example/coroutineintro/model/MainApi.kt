package com.example.coroutineintro.model

interface MainApi {
    suspend fun getDisplayTopics(): HashMap<String, String>
    suspend fun getRandomColor(): Int
}