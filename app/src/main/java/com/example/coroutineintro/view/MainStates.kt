package com.example.coroutineintro.view

data class MainStates(
    var enableNavButtons: Boolean = true,
    var enableColorSpecific: Boolean = false,
    var enableLifecycleSpecific: Boolean = false,
    var isLoading: Boolean = false
)
