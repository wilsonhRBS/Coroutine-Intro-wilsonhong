package com.example.coroutineintro.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.coroutineintro.R
import com.example.coroutineintro.databinding.LifecycleFragmentBinding
import com.example.coroutineintro.viewmodel.MainViewModel

class LifecycleFragment: Fragment() {
    private var _binding: LifecycleFragmentBinding? = null
    private val binding: LifecycleFragmentBinding get() = _binding!!
    private val vm by viewModels<MainViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = LifecycleFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        vm.state.observe(viewLifecycleOwner) {
            stateHandler(it)
        }
        vm.studyTopics.observe(viewLifecycleOwner) {
            ArrayAdapter<String>(requireContext(), android.R.layout.simple_spinner_item, it.keys.toList()).apply {
                setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.lifecycleSpinner.adapter = this
            }
        }
    }

    private fun stateHandler(state: MainStates) = with(binding) {
        btnRetrieve.isEnabled = state.enableNavButtons
        btnBack.isEnabled = state.enableNavButtons
        btnNext.isEnabled = state.enableNavButtons
        btnConfirm.isEnabled = state.enableLifecycleSpecific
        progress.isVisible = state.isLoading
    }

    private fun initViews() = with(binding) {
        btnBack.setOnClickListener { findNavController().navigateUp() }
        btnNext.setOnClickListener { findNavController().navigateUp() }
        btnRetrieve.setOnClickListener {
            vm.getTopics()
        }
        btnConfirm.setOnClickListener {
            val name: String = lifecycleSpinner.selectedItem.toString()
            //Toast.makeText(requireContext(), name, Toast.LENGTH_SHORT).show()
            val desc:String = vm.getDesc(name)
            //Toast.makeText(requireContext(), desc, Toast.LENGTH_SHORT).show()
            findNavController().navigate(
                LifecycleFragmentDirections.actionLifecycleFragmentToLifecycleDisplayFragment(name, desc)
            )
        }
    }
}