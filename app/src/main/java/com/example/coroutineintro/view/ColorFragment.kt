package com.example.coroutineintro.view

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.coroutineintro.databinding.ColorFragmentBinding
import com.example.coroutineintro.viewmodel.MainViewModel

class ColorFragment: Fragment() {
    private var _binding: ColorFragmentBinding? = null
    private val binding: ColorFragmentBinding get() = _binding!!
    private val vm by viewModels<MainViewModel>()

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ColorFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
        vm.state.observe(viewLifecycleOwner) {
            stateHandler(it)
        }
        vm.color.observe(viewLifecycleOwner) {
            binding.btnDisplay.setBackgroundColor(it)
        }
    }

    private fun stateHandler(state: MainStates) = with(binding) {
        btnRetrieve.isEnabled = state.enableNavButtons
        btnBack.isEnabled = state.enableNavButtons
        btnNext.isEnabled = state.enableNavButtons
        btnDisplay.isEnabled = state.enableColorSpecific
        progress.isVisible = state.isLoading
    }

    private fun initViews() = with(binding) {
        btnRetrieve.setOnClickListener {
            vm.getRandomColor()
        }
        btnBack.setOnClickListener {
            val action = ColorFragmentDirections.actionColorFragmentToLifecycleFragment()
            findNavController().navigate(action)
        }
        btnNext.setOnClickListener {
            val action = ColorFragmentDirections.actionColorFragmentToLifecycleFragment()
            findNavController().navigate(action)
        }
        btnDisplay.setOnClickListener {
            val bkColor = (btnDisplay.background as ColorDrawable).color
            val action = ColorFragmentDirections.actionColorFragmentToSpecificColorFragment(bkColor)
            findNavController().navigate(action)
        }
    }
}