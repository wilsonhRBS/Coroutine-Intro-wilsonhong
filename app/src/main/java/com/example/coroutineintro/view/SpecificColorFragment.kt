package com.example.coroutineintro.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.coroutineintro.databinding.ColorSpecificFragmentBinding

class SpecificColorFragment: Fragment() {
    private var _binding: ColorSpecificFragmentBinding? = null
    private val binding: ColorSpecificFragmentBinding get() = _binding!!
    private val args by navArgs<SpecificColorFragmentArgs>()

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = ColorSpecificFragmentBinding.inflate(inflater, container, false).also {
        _binding = it
    }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews()
    }

    private fun initViews() = with(binding) {
        tvDescription.setBackgroundColor(args.color)
        btnClose.setOnClickListener {
            findNavController().navigateUp()
        }
    }
}